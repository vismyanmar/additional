-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: thapyayni
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brand` (
  `Brand_ID` int NOT NULL,
  `Brand_Name` varchar(10) NOT NULL,
  `Brand_Remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Brand_ID`),
  UNIQUE KEY `Brand_ID_UNIQUE` (`Brand_ID`),
  CONSTRAINT `Country_ID` FOREIGN KEY (`Brand_ID`) REFERENCES `country` (`Country_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `Country_ID` int NOT NULL,
  `Country_Code` varchar(10) NOT NULL,
  `Country_Remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Country_ID`),
  UNIQUE KEY `Country_ID_UNIQUE` (`Country_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cutomer`
--

DROP TABLE IF EXISTS `cutomer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cutomer` (
  `Customer_ID` int NOT NULL,
  `Customer_Name` varchar(50) NOT NULL,
  `Customer_Address` varchar(50) DEFAULT NULL,
  `Customer_Phone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Customer_ID`),
  UNIQUE KEY `Customer_ID_UNIQUE` (`Customer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item` (
  `Item_ID` int NOT NULL,
  `Item_Code` varchar(50) NOT NULL,
  `Item_Name` varchar(50) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Price_Purchased` double NOT NULL,
  `Price_Selling` double NOT NULL,
  `Price_Customer` double NOT NULL,
  `Quantity` int NOT NULL,
  `Customized_Quantity` int NOT NULL,
  `Minimum` int NOT NULL,
  PRIMARY KEY (`Item_ID`),
  UNIQUE KEY `Item_ID_UNIQUE` (`Item_ID`),
  CONSTRAINT `Brand_ID` FOREIGN KEY (`Item_ID`) REFERENCES `brand` (`Brand_ID`),
  CONSTRAINT `Unit_ID` FOREIGN KEY (`Item_ID`) REFERENCES `unit` (`Unit_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sales_order` (
  `Sales_Order_ID` int NOT NULL,
  `Invoice_Code` varchar(11) NOT NULL,
  `Sales_Order_Date_Time` datetime DEFAULT NULL,
  `Total_Price` double NOT NULL,
  `Left_To_Be_Paid` double DEFAULT NULL,
  `Paid_Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Sales_Order_ID`),
  UNIQUE KEY `Sales_Order_ID_UNIQUE` (`Sales_Order_ID`),
  CONSTRAINT `Customer_ID` FOREIGN KEY (`Sales_Order_ID`) REFERENCES `cutomer` (`Customer_ID`),
  CONSTRAINT `Sales_Order_Type_ID` FOREIGN KEY (`Sales_Order_ID`) REFERENCES `sales_order_type` (`Sales_Order_Type_ID`),
  CONSTRAINT `Vendor_ID` FOREIGN KEY (`Sales_Order_ID`) REFERENCES `vendor` (`Vendor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sales_order_type`
--

DROP TABLE IF EXISTS `sales_order_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sales_order_type` (
  `Sales_Order_Type_ID` int NOT NULL,
  `Sales_Order_Type` varchar(10) NOT NULL,
  PRIMARY KEY (`Sales_Order_Type_ID`),
  UNIQUE KEY `Sales_Order_Type_ID_UNIQUE` (`Sales_Order_Type_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shelf`
--

DROP TABLE IF EXISTS `shelf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shelf` (
  `Shelf_ID` int NOT NULL,
  `Shelf_Name` varchar(10) NOT NULL,
  `Shelf_Remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Shelf_ID`),
  UNIQUE KEY `Shelf_ID_UNIQUE` (`Shelf_ID`),
  CONSTRAINT `Warehouse_ID` FOREIGN KEY (`Shelf_ID`) REFERENCES `warehouse` (`Warehouse_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unit` (
  `Unit_ID` int NOT NULL,
  `Unit_Code` varchar(10) NOT NULL,
  `Unit_Name` varchar(50) NOT NULL,
  `Unit_Remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Unit_ID`),
  UNIQUE KEY `Unit_ID_UNIQUE` (`Unit_ID`),
  CONSTRAINT `Unit_Category_ID` FOREIGN KEY (`Unit_ID`) REFERENCES `unit_category` (`Unit_Category_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unit_category`
--

DROP TABLE IF EXISTS `unit_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unit_category` (
  `Unit_Category_ID` int NOT NULL,
  `Unit_Category_Name` varchar(50) NOT NULL,
  `Unit_Category_Remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Unit_Category_ID`),
  UNIQUE KEY `Unit_Category_ID_UNIQUE` (`Unit_Category_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor` (
  `Vendor_ID` int NOT NULL,
  `Vendor_Name` varchar(50) NOT NULL,
  `Customer_Address` varchar(50) DEFAULT NULL,
  `Customer_Phone` varchar(50) DEFAULT NULL,
  `Vendor_Photo` blob,
  PRIMARY KEY (`Vendor_ID`),
  UNIQUE KEY `Vendor_ID_UNIQUE` (`Vendor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse` (
  `Warehouse_ID` int NOT NULL,
  `Warehouse_Name` varchar(50) NOT NULL,
  `Warehouse_Remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Warehouse_ID`),
  UNIQUE KEY `Warehouse_ID_UNIQUE` (`Warehouse_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-18 21:49:23
